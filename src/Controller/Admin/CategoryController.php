<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CategoryController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/categories")
 */
class CategoryController extends AbstractController {
    /** @Route("",name="admin_categories") */
    public function index(CategoryRepository $categoryRepository, RouterInterface $router): Response {
        $form = $this->createForm(CategoryType::class, new Category(), ["action" => $router->generate("admin_add_category")]);
        $categories = $categoryRepository->findAll();
        return $this->render("admin/categories/categories.twig", ["form" => $form->createView(), 'categories' => $categories]);
    }

    /** @Route("/add",name="admin_add_category",methods={"POST"}) */
    public function add(Request $request, CategoryRepository $repository, Str $str) {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category->setSlug($str->slug($category->getTitle()));
            $repository->save($category);
            $this->addFlash('success', "Catégorie ajoutée avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_categories');
    }

    /** @Route("/edit/{uid}",name="admin_edit_category") */
    public function edit(Request $request, Category $category, CategoryRepository $categoryRepository, RouterInterface $router, Str $str): Response {
        $form = $this->createForm(CategoryType::class, $category, ["action" => $router->generate("admin_edit_category",['uid'=>$category->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category->setSlug($str->slug($category->getTitle()));
            $categoryRepository->save($category);
            $this->addFlash('success', "Catégorie modifiée avec succès");
            return $this->redirectToRoute('admin_categories');
        }
        $categories = $categoryRepository->findAll();
        return $this->render("admin/categories/categories.twig", ["form" => $form->createView(), 'categories' => $categories]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_category")
     */
    public function delete(Category $category, CategoryRepository $categoryRepository): Response {
        $categoryRepository->remove($category);
        $this->addFlash('success', "Catégorie supprimée avec succès");
        return $this->redirectToRoute('admin_categories');
    }
}