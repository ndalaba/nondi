<?php

/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:06 PM
 */

namespace App\Controller\Admin;


use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\PageRepository;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class IndexController
 * @package App\Controller\Admin
 * @Route("/admin")
 */
class IndexController extends AbstractController {

    /** @Route("/index",name="admin_home") */
    public function index(ArticleRepository $articleRepository, VideoRepository $videoRepository, PageRepository $pageRepository, UserRepository $userRepository,RouterInterface $router) {
        $authors = $userRepository->findAll();
        $form = $this->createForm(ArticleType::class,(new Article()),['action'=>$router->generate("admin_add_article")]);
        if ($this->getUser()->hasRole('ROLE_ADMIN')) {
            $articles_published = $articleRepository->count(['published' => true]);
            $videos_published = $videoRepository->count(['published' => true]);
            $users_activated = $userRepository->count(['activated' => true]);
            $page_activated = $pageRepository->count(['published' => true]);
            $users = $userRepository->findBy(['activated' => false], ["createdAt" => "DESC"]);
            $output = ['articles' => $articles_published, 'videos' => $videos_published, 'users_activated' => $users_activated, 'pages' => $page_activated,'users'=>$users, 'authors' => $authors, 'form' => $form->createView()];
        } else {
            $articles_published = $articleRepository->count(['published' => true, 'user' => $this->getUser()]);
            $videos_published = $videoRepository->findBy(['published' => true, 'user' => $this->getUser()]);
            $output = ['articles' => $articles_published, 'videos' => $videos_published, 'form' => $form->createView()];
        }

        return $this->render('admin/home.twig', $output);
    }
}
