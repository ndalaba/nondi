<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Utils\EmailManager;
use App\Utils\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MessageController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/messages")
 */
class MessageController extends AbstractController {
    /** @Route("",name="admin_messages") */
    public function index(Request $request, MessageRepository $messageRepository, Pagination $pagination): Response {
        $page = $request->get('page', 1);
        $action = $request->query->get('doaction', '');
        if ($action == "Appliquer") {
            $todo = intval($request->get('todo'));
            if ($todo == -1) $messageRepository->removeBy(['uid' => $request->get('uid')]);
            return $this->redirectToRoute('admin_messages');
        }
        $folder = $request->get('folder', Message::MESSAGE_TYPE['INBOX']);
        $messages = $messageRepository->lastMessages($folder, $page - 1);
        $pagination = $pagination->getPagination('admin_messages', $messageRepository->count(['folder' => $folder]));
        return $this->render("admin/messages/index.twig", ['messages' => $messages, 'pagination' => $pagination]);
    }

    /**
     * @Route("/detail",name="admin_messages_detail")
     */
    public function show(Request $request,MessageRepository $messageRepository) {
        $message=$messageRepository->findOneBy(['uid'=>$request->get('uid')]);
        $message->setRead(true);
        $messageRepository->save($message);
        return $this->render('admin/messages/detail.twig', ['message' => $message]);
    }

    /** @Route("/compose",name="admin_add_message") */
    public function compose(Request $request, MessageRepository $repository, EmailManager $emailManager) {
        $message = new Message();
        $message->setEmailFrom($this->getParameter('app_email'))->setFolder(Message::MESSAGE_TYPE['SENT']);
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $renderEmail = $this->renderView('email/contact.twig', ['message' => $message->getMessage()]);
            $emailManager->sendMail($message->getEmailFrom(), $message->getEmailTo(), $message->getSubject(), $renderEmail);
            $repository->save($message);
            $this->addFlash('success', "Message envoyé avec succès");
            return $this->redirectToRoute('admin_messages');
        }
        return $this->render('admin/messages/compose.twig', ['form' => $form->createView()]);
    }

    /** @Route("/respond/{uid}",name="admin_respond_message") */
    public function respond(Request $request, Message $message, MessageRepository $repository, EmailManager $emailManager) {
        $newMessage = new Message();
        $newMessage->setEmailFrom($message->getEmailTo())->setEmailTo($message->getEmailFrom())->setSubject('Re: ' . $message->getSubject())->setFolder(Message::MESSAGE_TYPE['SENT']);
        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $renderEmail = $this->renderView('email/contact.twig', ['message' => $message->getMessage()]);
            $emailManager->sendMail($message->getEmailFrom(), $message->getEmailTo(), $message->getSubject(), $renderEmail);
            $repository->save($message);
            $this->addFlash('success', "Message envoyé avec succès");
            return $this->redirectToRoute('admin_messages', ['folder' => Message::MESSAGE_TYPE['SENT']]);
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->render('admin/messages/compose.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_message")
     */
    public function delete(Message $message, MessageRepository $messageRepository): Response {
        $messageRepository->remove($message);
        $this->addFlash('success', "Message supprimé avec succès");
        return $this->redirectToRoute('admin_messages');
    }
}