<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Page;
use App\Form\PageType;
use App\Repository\PageRepository;
use App\Utils\FileUploader;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PageController
 * @package App\Controller\Admin
 * @Route("/admin/pages")
 * @IsGranted("ROLE_ADMIN")
 */
class PageController extends AbstractController {
    /**
     * @Route("",name="admin_pages")
     */
    public function index(PageRepository $pageRepository, RouterInterface $router): Response {
        $form = $this->createForm(PageType::class, new Page(), ["action" => $router->generate("admin_add_page")]);
        return $this->render("admin/pages/pages.twig", ["form" => $form->createView(), 'pages' => $pageRepository->findAll()]);
    }

    /** @Route("/add",name="admin_add_page",methods={"POST"}) */
    public function add(Request $request, PageRepository $repository, Str $str, FileUploader $uploader) {
        $page = new Page();
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $page->setSlug($str->slug($page->getTitle()));
            $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'pages');
            if ($response['uploaded'])
                $page->setImage($response['fileName']);
            $repository->save($page);
            $this->addFlash('success', "Page ajoutée avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_pages');
    }

    /** @Route("/edit/{uid}",name="admin_edit_page") */
    public function edit(Request $request, Page $page, PageRepository $pageRepository, RouterInterface $router, Str $str, FileUploader $uploader): Response {
        $form = $this->createForm(PageType::class, $page, ["action" => $router->generate("admin_edit_page", ['uid' => $page->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $page->setSlug($str->slug($page->getTitle()));
            if ($page->getImage()) {
                $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('image')->getData(), 'pages');
                if ($response['uploaded'])
                    $page->setImage($response['fileName']);
            } else
                $page->setImage($request->get('previous_image'));
            $pageRepository->save($page);
            $this->addFlash('success', "Page modifiée avec succès");
            return $this->redirectToRoute('admin_pages');
        }
        return $this->render('admin/pages/pages.twig', ['form' => $form->createView(), 'pages' => $pageRepository->findAll(), 'page' => $page]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_page")
     */
    public function delete(Page $page, PageRepository $pageRepository): Response {
        $pageRepository->remove($page);
        $this->addFlash('success', "Page supprimée avec succès");
        return $this->redirectToRoute('admin_pages');
    }
}