<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Utils\FileUploader;
use App\Utils\Pagination;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class UserController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/users")
 */
class UserController extends AbstractController {

    /** @Route("/profils/{uid}",name="admin_profils") */
    public function profils(RouterInterface $router, User $user): Response {
        $form = $this->createForm(UserType::class, $user, ["action" => $router->generate("admin_edit_profil", ['uid' => $user->getUid()])]);
        return $this->render("admin/profils/profil.twig", ["form" => $form->createView()]);
    }

    /** @Route("",name="admin_users") */
    public function index(Request $request, UserRepository $userRepository, Pagination $pagination): Response {
        $page = $request->get('page', 1);
        $action = $request->query->get('doaction', '');
        if ($action == "Appliquer") {
            $todo = intval($request->get('todo'));
            if ($todo == -1) $userRepository->removeBy(['uid' => $request->get('uid')]);
            if ($todo == 1) $userRepository->activate($request->get('uid'));
            if ($todo == 0) $userRepository->deActivate($request->get('uid'));
            return $this->redirectToRoute('admin_users');
        } elseif ($action == "Filtrer") {
            $data = ["doaction" => "Filtrer"];
            $email = $request->get('email');
            $activated = $request->get('active');
            if (trim($email) !== "") $data['email'] = $email;
            if (trim($activated) !== "") $data['activated'] = intval($activated);
            $users = $userRepository->filter($data, ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_users', $userRepository->count([]), $page, $data);
            return $this->render("admin/users/users.twig", ['users' => $users, 'pagination' => $pagination]);
        } else {
            $users = $userRepository->last($this->getUser(), ($page - 1), Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('admin_articles', $userRepository->count([]), $page);
            return $this->render("admin/users/users.twig", ['users' => $users, 'pagination' => $pagination]);
        }

    }

    /** @Route("/add",name="admin_add_user",methods={"POST"}) */
    public function add(Request $request, UserRepository $repository) {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->save($user);
            $this->addFlash('success', "Membre ajouté avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_users');
    }

    /** @Route("/edit/password", name="admin_edit_password") */
    public function editPassword(Request $request, UserRepository $repository, Str $str) {
        if ($request->isMethod('POST')) {
            $password = $request->get('password');
            $password_confirm = $request->get('password_confirm');
            if ($password === $password_confirm) {
                $user = $this->getUser();
                $user->setPlainPassword($password)->setPassword($str->encodePassword($user));
                $repository->save($user);
                $this->addFlash('success', "Mot de passe modifé.");
            } else $this->addFlash('error', "La confirmation du mot de passe ne correspond pas au mot de passe.");
        }
        return $this->redirectToRoute('admin_profils',['uid'=>$this->getUser()->getUid()]);
    }

    /** @Route("/edit/{uid}",name="admin_edit_profil") */
    public function edit(Request $request, User $user, UserRepository $userRepository, RouterInterface $router, FileUploader $uploader): Response {
        $form = $this->createForm(UserType::class, $user, ["action" => $router->generate("admin_edit_profil", ['uid' => $user->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($user->getPhoto()) {
                    $response = $uploader->setUploadPath($this->getParameter('upload_path'))->upload($form->get('photo')->getData(), 'users');
                    if ($response['uploaded'])
                        $user->setPhoto($response['fileName']);
                } else
                    $user->setPhoto($request->get('previous_photo'));
                $userRepository->save($user);
                $this->addFlash('success', "Profils modifié avec succès");
                return $this->redirectToRoute('admin_profils', ['uid' => $user->getUid()]);
            } else {
                $form->get('photo')->setData($user->getPhoto());
                $this->addFlash('error', $form->getErrors());
            }
        }
        return $this->render("admin/profils/profil.twig", ["form" => $form->createView()]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_user")
     */
    public function delete(User $user, UserRepository $userRepository): Response {
        if (!$user->hasRole('ROLE_ADMIN')) {
            $userRepository->remove($user);
            $this->addFlash('success', "Utilisateur supprimé avec succès.");
        } else  $this->addFlash('error', "Impossible de supprimer cet utilisateur.");

        return $this->redirectToRoute('admin_users');
    }
}