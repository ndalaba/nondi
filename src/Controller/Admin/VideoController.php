<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 3:37 PM
 */

namespace App\Controller\Admin;


use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use App\Utils\Pagination;
use App\Utils\Str;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class VideoController
 * @package App\Controller\Admin
 * @Route("/admin/videos")
 */
class VideoController extends AbstractController {
    /** @Route("",name="admin_videos") */
    public function index(Request $request, VideoRepository $videoRepository, RouterInterface $router, Pagination $pagination): Response {
        $form = $this->createForm(VideoType::class, new Video(), ["action" => $router->generate("admin_add_video")]);
        $page = $request->get('page', 1);
        $videos = $videoRepository->last($this->getUser(),($page - 1));
        $pagination = $pagination->getPagination('admin_videos', $videoRepository->count([]), $page);
        return $this->render("admin/videos/videos.twig", ["form" => $form->createView(), 'videos' => $videos, 'pagination' => $pagination]);
    }

    /** @Route("/add",name="admin_add_video",methods={"POST"}) */
    public function add(Request $request, VideoRepository $repository, Str $str) {
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $video->setSlug($str->slug($video->getTitle()));
            $repository->save($video);
            $this->addFlash('success', "Vidéo ajoutée avec succès");
        } else
            $this->addFlash('error', "Erreur formulaire");
        return $this->redirectToRoute('admin_videos');
    }

    /** @Route("/edit/{uid}",name="admin_edit_video") */
    public function edit(Request $request, Video $video, VideoRepository $videoRepository, RouterInterface $router, Pagination $pagination, Str $str): Response {
        $form = $this->createForm(VideoType::class, $video, ["action" => $router->generate("admin_edit_video", ['uid' => $video->getUid()])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $video->setSlug($str->slug($video->getTitle()));
            $videoRepository->save($video);
            $this->addFlash('success', "Vidéo modifiée avec succès");
            return $this->redirectToRoute('admin_videos');
        }
        $page = $request->get('page', 1);
        $videos = $videoRepository->last($this->getUser(),($page - 1));
        $pagination = $pagination->getPagination('admin_videos', $videoRepository->count([]), $page);
        return $this->render('admin/videos/videos.twig', ['form' => $form->createView(), 'videos' => $videos, 'pagination' => $pagination]);
    }

    /**
     * @Route("/delete/{uid}",name="admin_delete_video")
     */
    public function delete(Video $video, VideoRepository $videoRepository): Response {
        $videoRepository->remove($video);
        $this->addFlash('success', "Vidéo supprimée avec succès");
        return $this->redirectToRoute('admin_videos');
    }
}