<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 15:21
 */

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Repository\MessageRepository;
use App\Repository\PageRepository;
use App\Repository\PersonRepository;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use App\Utils\EmailManager;
use App\Utils\Pagination;
use App\Utils\Str;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class HomeController extends AbstractController {
    /** @Route("", name="front_home") */
    public function home(VideoRepository $videoRepository, ArticleRepository $articleRepository): Response {
        $videos = $videoRepository->lastPublished(0, 6);
        $divers = $articleRepository->findBy(['category' => 8, 'published' => 1], ['createdAt' => "DESC"], 3, 0);
        $blogs = $articleRepository->findBy(['category' => 7, 'published' => 1, 'top' => 0], ['createdAt' => "DESC"], 4, 0);
        $blog = $articleRepository->findBy(['category' => 7, 'published' => 1, 'top' => 1], ['createdAt' => "DESC"], 1, 0);
        $articles = $articleRepository->findBy(['category' => 6, 'published' => 1, 'top' => 0], ['createdAt' => "DESC"], 4, 0);
        $article = $articleRepository->findBy(['category' => 6, 'published' => 1, 'top' => 1], ['createdAt' => "DESC"], 1, 1);
        $tops = $articleRepository->findBy(['category' => 6, 'published' => 1, 'top' => 1], ['createdAt' => "DESC"], 1, 1);
        $output = ['videos' => $videos, 'divers' => $divers, 'blogs' => $blogs, 'blog' => $blog, 'articles' => $articles, 'article' => $article, 'tops' => $tops];
        return $this->render("front/home.twig", $output);
    }

    /** @Route("/rechercher", name="front_rechercher") */
    public function rechercher(Request $request, ArticleRepository $articleRepository, Pagination $pagination): Response {
        $page = intval($request->get('page', 1));
        $q = $request->get('q');
        $articles = $articleRepository->frontSearch($q, ($page - 1), Pagination::PER_PAGE);
        $pagination = $pagination->getPagination('front_rechercher', $articleRepository->countFrontSearch($q), $page);
        return $this->render('front/articles.twig', ['articles' => $articles, 'pagination' => $pagination, 'q' => $q]);
    }

    /** @Route("/videos",name="front_videos") */
    public function videos(Request $request, VideoRepository $videoRepository, Pagination $pagination): Response {
        $page = $request->get('page', 1);
        $slug = $request->get('slug');
        $video = $videoRepository->findOneBy(['slug' => $slug]);
        if ($video != null) {
            $video->setVue($video->getVue()+1);
            $videoRepository->save($video);
            $videos = $videoRepository->lastPublished(($page - 1)*Pagination::PER_PAGE, Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('front_videos', $videoRepository->count(['published' => 1]), $page, ['slug' => $slug]);
            return $this->render('front/videos.twig', ['videos' => $videos, 'video' => $video, 'pagination' => $pagination]);
        } else return $this->redirectToRoute('front_articles');
    }

    /** @Route("/{cat_slug}/dossiers",name="front_articles") */
    public function articles(Request $request, string $cat_slug, CategoryRepository $categoryRepository, ArticleRepository $articleRepository, Pagination $pagination) {
        $page = $request->get('page', 1);
        $category = $categoryRepository->findOneBy(['slug' => $cat_slug]);
        if ($category != null) {
            $articles = $articleRepository->findBy(['published' => 1, 'category' => $category], ['createdAt' => "DESC"], Pagination::PER_PAGE, ($page - 1)*Pagination::PER_PAGE);
            $pagination = $pagination->getPagination('front_articles', $articleRepository->count(['published' => 1, 'category' => $category]), $page, ['cat_slug' => $cat_slug]);
            return $this->render('front/articles.twig', ['articles' => $articles, 'pagination' => $pagination, 'category' => $category]);
        } else return $this->redirectToRoute('front_home');
    }

    /** @Route("/nondi/{slug}",name="front_page") */
    public function page(string $slug, PageRepository $pageRepository, PersonRepository $personRepository) {
        $page = $pageRepository->findOneBy(['slug' => $slug]);
        if ($page != null) {
            $persons = $personRepository->findBy(['published' => 1]);
            return $this->render('front/page.twig', ['page' => $page, 'persons' => $persons]);
        } else return $this->redirectToRoute('front_home');
    }


    /** @Route("/collaborateurs/{slug}",name="front_persons") */
    public function persons(string $slug, PersonRepository $personRepository) {
        $person = $personRepository->findOneBy(['slug' => $slug]);
        if ($person != null) {
            $persons = $personRepository->findBy(['published' => 1]);
            return $this->render('front/person.twig', ['person' => $person, 'persons' => $persons]);
        } else return $this->redirectToRoute('front_home');
    }

    /** @Route("/{cat_slug}/{slug}",name="front_article") */
    public function article(string $cat_slug, string $slug, ArticleRepository $articleRepository) {
        $article = $articleRepository->findOneBy(['slug' => $slug]);
        if ($article !== null) {
            $article->setVue($article->getVue() + 1);
            $articleRepository->save($article);
            $articles = $articleRepository->filter(['published' => 1], null, 0, 3);
            return $this->render('front/article.twig', ['article' => $article, 'articles' => $articles]);
        } else return $this->redirectToRoute('front_home');
    }


    /** @Route("/contact",name="front_contact") */
    public function contact(Request $request, MessageRepository $messageRepository, EmailManager $emailManager) {
        if ($request->isMethod('post')) {
            $message = new Message();
            $message->setUser(null)->setFolder(Message::MESSAGE_TYPE['INBOX'])->setEmailTo($this->getParameter('app_email'))
                ->setEmailFrom($request->get('email'))->setSubject($request->get('subject'))->setName($request->get('name'))
                ->setMessage($request->get('message'))->setPhone($request->get('phone'))->setRead(false);
            $messageRepository->save($message);
            $renderEmail = $this->renderView('email/contact.twig', ['message' => $message->getMessage()]);
            $emailManager->sendMail($message->getEmailFrom(), $message->getEmailTo(), $message->getSubject(), $renderEmail);
            return $this->json(['type' => "success", 'text' => "Votre message a été envoyé"]);
        }
        return $this->render('front/contact.twig');
    }

    /** @Route("/s-enregistrer",name="front_register") */
    public function register(Request $request, UserRepository $userRepository, Str $str, ValidatorInterface $validator) {
        if ($request->isMethod('post')) {
            $user = new User();
            $user->setRoles(['ROLE_USER'])->setName($request->get('name'))->setActivated(false)->setEmail($request->get('email'))
                ->setFacebook($request->get('facebook'))->setPhone($request->get('phone'))->setPlainPassword($request->get('password'))->setLocation($request->get('location'));
            $user->setPassword($str->encodePassword($user));
            $errors = $validator->validate($user);
            if (count($validator->validate($user)))
               return $this->json(['type' => 'error', "text" => (string)$errors]);
            else {
                $userRepository->save($user);
                return $this->json(['type' => 'success', "text" => "Votre compte est en attente de validation"]);
            }
        }
        return $this->render('front/register.twig');
    }
}