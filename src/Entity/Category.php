<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/12/19
 * Time: 7:25 PM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="categories")
 */
class Category extends Entity {

    const CAT_PER_PAGE = 5;

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var ArrayCollection | Article[]
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category")
     */
    private $articles;


    /**
     * @var ArrayCollection | Video[]
     * @ORM\OneToMany(targetEntity="Video", mappedBy="category")
     */
    private $videos;


    public function __construct(string $title = "", string $slug = "") {
        $this->setUid()->setTitle($title)->setSlug($slug);
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Category
     */
    public function setTitle(string $title): Category {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Category
     */
    public function setSlug(string $slug): Category {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Category
     */
    public function setDescription(string $description): Category {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Article[]|ArrayCollection
     */
    public function getArticles() {
        return $this->articles;
    }

    /**
     * @param Article[]|ArrayCollection $articles
     * @return Category
     */
    public function setArticles($articles) {
        $this->articles = $articles;
        return $this;
    }

    /**
     * @return Video[]|ArrayCollection
     */
    public function getVideos() {
        return $this->videos;
    }

    /**
     * @param Video[]|ArrayCollection $videos
     * @return Category
     */
    public function setVideos($videos) {
        $this->videos = $videos;
        return $this;
    }

    public function __toString(): string {
        return $this->title;
    }

}