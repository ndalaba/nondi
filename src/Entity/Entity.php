<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 12:16
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\MappedSuperclass
 */
abstract class Entity
{

    /**
     * @var string
     * @ORM\Column(name="uid",nullable=false,unique=true, length=191)
     */
    protected $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var boolean
     * @ORM\Column(name="published", type="boolean")
     */
    protected $published=0;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @throws \Exception
     */
    public function updatedTimestamps()
    {
        $this->updatedAt = new \DateTime('now');
        if ($this->createdAt == null) {
            $this->createdAt = new \DateTime('now');
        }
    }

    /**
     * @return mixed
     */
    public function getCreatedAt():\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     *
     * @return Entity
     */
    public function setCreatedAt(\DateTime $createdAt):self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return Entity
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return $this
     */
    public function setUid():self
    {
        $this->uid = uniqid();

        return $this;
    }
    
    public function isPublished():bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published= $published;
        return $this;
    }
}
