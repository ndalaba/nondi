<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Message
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="messages")
 */
class Message extends Entity {

    const MESSAGE_TYPE = ['INBOX' => 'inbox', 'SENT' => 'sent', 'DRAFT' => 'draft', 'STRASH' => 'strash'];

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Email(message="Addresse email incorrecte")
     */
    private $email_from;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Email(message="Addresse email incorrecte")
     */
    private $email_to;

    /**
     * @var string
     * @ORM\Column(name="subject", type="string", length=191, nullable=false)
     * @Assert\NotBlank(message="Le champ sujet ne doit pas être vide")
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;
    /**
     * @var string
     * @ORM\Column(type="string", length=191,nullable=true)
     */
    private $name="Membre Nondi";
    /**
     * @var string
     * @ORM\Column(type="string", length=191,nullable=true)
     */
    private $phone="555-555-555";
    /**
     * @var boolean
     * @ORM\Column(type="boolean",name="lu")
     */
    private $read=false;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $folder;// INBOX, SENT, DRAFT, STRASH
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_id",nullable=true)
     */
    private $user;

    public function __construct() {
        $this->setUid();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Message
     */
    public function setId(int $id): Message {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailFrom(): ?string {
        return $this->email_from;
    }

    /**
     * @param string $email_from
     * @return Message
     */
    public function setEmailFrom(string $email_from): Message {
        $this->email_from = $email_from;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailTo(): ?string {
        return $this->email_to;
    }

    /**
     * @param string $email_to
     * @return Message
     */
    public function setEmailTo(string $email_to): Message {
        $this->email_to = $email_to;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): ?string {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return Message
     */
    public function setSubject(string $subject): Message {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Message
     */
    public function setMessage(string $message): Message {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Message
     */
    public function setName(string $name): Message {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Message
     */
    public function setPhone($phone): Message {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRead(): ?bool {
        return $this->read;
    }

    /**
     * @param boolean $read
     * @return Message
     */
    public function setRead(bool $read): Message {
        $this->read = $read;
        return $this;
    }

    /**
     * @return string
     */
    public function getFolder(): ?string {
        return $this->folder;
    }

    /**
     * @param string $folder
     * @return Message
     */
    public function setFolder(string $folder): Message {
        $this->folder = $folder;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Message
     */
    public function setUser(?User $user): Message {
        $this->user = $user;
        return $this;
    }
}
