<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Page
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="pages")
 */
class Page extends Entity {

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     * @Assert\Image(mimeTypes = {"image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Les formats d'images acceptés sont jpg, png, gif")
     */
    private $image="noimage.png";

    public function __construct(string $title = "", string $slug = "") {
        $this->setUid()->setTitle($title)->setSlug($slug);
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Page
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Page
     */
    public function setTitle(string $title): Page {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Page
     */
    public function setSlug(string $slug): Page {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Page
     */
    public function setContent(string $content): Page {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Page
     */
    public function setImage($image): Page {
        $this->image = $image;
        return $this;
    }

    public function getExtrait(int $len = 150): string {
        return substr(strip_tags($this->getContent()), 0, $len);
    }

}