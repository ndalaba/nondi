<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/12/19
 * Time: 7:25 PM
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Class Category
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="videos")
 */
class Video extends Entity {

    /** @ORM\Id @ORM\GeneratedValue(strategy="AUTO") @ORM\Column(name="id", type="integer", nullable=false) */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=191)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=191,nullable=false)
     */
    private $slug;
    /**
     * @var string
     * @ORM\Column(type="string",length=191)
     */
    private $video;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="videos")
     */
    private $user;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="videos")
     */
    private $category;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $vue=0;

    public function __construct(string $title = "", string $slug = "") {
        $this->setUid()->setTitle($title)->setSlug($slug);
    }

    public function getVideoId(){
        return substr($this->video,32);
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Video
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Video
     */
    public function setTitle(string $title): self {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Video
     */
    public function setSlug(string $slug): self {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getVideo(): ?string {
        return $this->video;
    }

    /**
     * @param string $video
     * @return Video
     */
    public function setVideo(string $video): self {
        $this->video = $video;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Video
     */
    public function setDescription(string $description): self {
        $this->description = $description;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Video
     */
    public function setUser(User $user): self {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Video
     */
    public function setCategory(Category $category): self {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getVue(): ?int {
        return $this->vue;
    }

    /**
     * @param int $vue
     * @return Video
     */
    public function setVue(int $vue): self {
        $this->vue = $vue;
        return $this;
    }

}



