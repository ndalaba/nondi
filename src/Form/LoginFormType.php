<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 14:19
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class LoginFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('_username', EmailType::class, array('label' => false, 'attr' => array('placeholder' => 'Adresse email','class'=>"form-control")))
            ->add('_password', PasswordType::class, array('label' => false, 'attr' => array('placeholder' => "Mot de passe","class"=>"form-control")));
    }
    public function getBlockPrefix() {
        return "";
    }
}
