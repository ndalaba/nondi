<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:34 PM
 */

namespace App\Repository;


use App\Entity\Article;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;


class ArticleRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Article::class);
    }

    public function frontSearch($title,int $start = 0, $end = Article::POSTS_PER_PAGE){
        $query = $this->createQueryBuilder('a');
        $query->andWhere('UPPER(a.title) LIKE :value')->setParameter('value', '%' . $title . '%')->andWhere('a.published=1');
        return $query->orderBy('a.createdAt', 'DESC')->setFirstResult($start)->setMaxResults($end)->getQuery()->getResult();
    }
    public function countFrontSearch($title){
        $query = $this->createQueryBuilder('a');
        $query->andWhere('UPPER(a.title) LIKE :value')->setParameter('value', '%' . $title . '%')->andWhere('a.published=1');
        return  count($query->getQuery()->getResult());
    }

    public function filter(array $critera, User $user = null, int $start = 0, $end = Article::POSTS_PER_PAGE) {
        if (!count($critera)) return $this->last();
        $query = $this->createQueryBuilder('a');
        foreach ($critera as $key => $value) {
            is_string($value) ? $query->andWhere('a.' . $key . " LIKE :value")->setParameter('value', '%' . $value . '%') : $query->andWhere('a.' . $key . '=:value')->setParameter('value', $value);
        }
        if ($user == null || $user->hasRole('ROLE_ADMIN'))
            return $query->orderBy('a.createdAt', 'DESC')->setFirstResult($start)->setMaxResults($end)->getQuery()->getResult();
        else return $query->andWhere("a.user=:user")->setParameter('user', $user)->orderBy('a.createdAt', 'DESC')->setFirstResult($start)->setMaxResults($end)->getQuery()->getResult();
    }

    public function count(array $critera, User $user = null) {
        if (!count($critera)) return parent::count($critera);
        $query = $this->createQueryBuilder('a');
        foreach ($critera as $key => $value) {
            is_string($value) ? $query->where('a.' . $key . " LIKE :value")->setParameter('value', '%' . $value . '%') : $query->where('a.' . $key . '=:value')->setParameter('value', $value);
        }
        if ($user == null || $user->hasRole('ROLE_ADMIN'))
            $result = $query->getQuery()->getResult();
        else $result = $query->andWhere("a.user=:user")->setParameter('user', $user)->getQuery()->getResult();

        return count($result);
    }

    public function populars() {
        return $this->createQueryBuilder('a')->where('a.published=1')->orderBy('a.vue' ,'DESC')->setFirstResult(0)->setMaxResults(3)->getQuery()->getResult();
    }

    public function publish(array $uids) {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.published=1 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }

    public function unpublish(array $uids) {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.published=0 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }

    public function setTop(array $uids) {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.top=1 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }

    public function setNormal(array $uids) {
        if (!count($uids) or empty($uids)) return;
        $sql = "UPDATE " . $this->_entityName . " Q SET Q.top=1 WHERE Q.uid IN(:uids) ";
        $query = $this->_em->createQuery($sql)->setParameter('uids', $uids);
        $query->execute();
    }
}