<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:34 PM
 */

namespace App\Repository;


use App\Entity\Category;
use Doctrine\Common\Persistence\ManagerRegistry;

class CategoryRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Category::class);
    }
}