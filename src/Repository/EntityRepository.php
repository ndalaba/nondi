<?php
/**
 * Created by PhpStorm.
 * User: mamadou.diallo
 * Date: 28/12/2018
 * Time: 14:21
 */

namespace App\Repository;

use App\Entity\Entity;
use App\Entity\User;
use App\Utils\Pagination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\ORMException;

abstract class EntityRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry, string $entityClass) {
        parent::__construct($registry, $entityClass);
    }

    public function last(User $user, int $start = 0, int $end = Pagination::PER_PAGE) {
        $query = $this->createQueryBuilder("a");
        if ($user == null || $user->hasRole('ROLE_ADMIN'))
            return $query->orderBy("a.createdAt", "DESC")
                ->setFirstResult($start)
                ->setMaxResults($end)
                ->getQuery()
                ->getResult();
        else
            return $query->where('a.user=:user')->setParameter('user', $user)->orderBy("a.createdAt", "DESC")
                ->setFirstResult($start)
                ->setMaxResults($end)
                ->getQuery()
                ->getResult();
    }

    public function lastPublished(int $start = 0, int $end = Pagination::PER_PAGE) {
        return $this->createQueryBuilder("a")
            ->where('a.published=1')
            ->orderBy("a.createdAt", "DESC")
            ->setFirstResult($start)
            ->setMaxResults($end)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $entity
     *
     * @return mixed
     */
    public function save($entity): Entity {
        try {
            $this->_em->persist($entity);
            $this->_em->flush();
        } catch (ORMException $e) {
        }

        return $entity;
    }


    /**
     * @param $entity
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove($entity) {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param $entities []
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeAll($entities) {
        foreach ($entities as $entity) {
            $this->_em->remove($entity);
        }
        $this->_em->flush();
    }

    public function findAll($order = []) {
        return $this->findBy([], $order);
    }

    /**
     * @param array $criteria
     */
    public function removeBy(array $criteria) {
        if (!count($criteria) or empty($criteria)) {
            return;
        }

        $sql = "DELETE FROM " . $this->_entityName . " Q WHERE 1=1 ";
        foreach ($criteria as $key => $value) {
            if (is_array($value)) {
                $sql .= " AND Q." . $key . " IN (:" . $key . ") ";
            } else {
                $sql .= " AND Q." . $key . " = :" . $key . " ";
            }
        }

        $query = $this->_em->createQuery($sql);
        foreach ($criteria as $key => $value) {
            $query->setParameter($key, $value);
        }
        $query->execute();
    }
}