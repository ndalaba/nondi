<?php
/**
 * Created by PhpStorm.
 * User: dmn
 * Date: 3/14/19
 * Time: 1:34 PM
 */

namespace App\Repository;


use App\Entity\Message;
use App\Entity\User;
use App\Utils\Pagination;
use Doctrine\Common\Persistence\ManagerRegistry;

class MessageRepository extends EntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Message::class);
    }

    public function unRead(User $user=null) {
        return $this->findBy(['read' => false, 'folder' => Message::MESSAGE_TYPE['INBOX']], ['createdAt' => "DESC"]);
    }

    public function lastMessages($messageType = Message::MESSAGE_TYPE['INBOX'], int $start = 0, int $end = Pagination::PER_PAGE) {
        $query = $this->createQueryBuilder("a");
        $query->where('a.folder=:folder')->setParameter('folder', $messageType);
        return $query->orderBy("a.createdAt", "DESC")
            ->setFirstResult($start)
            ->setMaxResults($end)
            ->getQuery()
            ->getResult();
    }
}